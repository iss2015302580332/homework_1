package iss.java.list;

/**
 * Created by wenke on 2016/9/16.
 * Modified by WeiZehao on 2016/9/28
 */
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Node
{
    private int data;
    private Node prev;
    private Node next;
    private MyList belong;                                                  // 指向Node所属的list
    public ReentrantReadWriteLock mylock = new ReentrantReadWriteLock();    // 读写锁

    public Node(MyList belong)
    {
        this.belong = belong;
    }

    public MyList getBelong()
    {
        return belong;
    }

    public int getData()
    {
        return data;
    }

    public Node setData(int data)
    {
        this.data = data;
        return this;
    }

    public Node getPrev()
    {
        return prev;
    }

    Node setPrev(Node prev)
    {
        this.prev = prev;
        return this;
    }

    public Node getNext()
    {
        return next;
    }

    Node setNext(Node next)
    {
        this.next = next;
        return this;
    }
}// class Node end
