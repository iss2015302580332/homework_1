package iss.java.list;

/**
 * Created by Weizehao on 16/10/7.
 */
public class ExceptionA extends Exception
{
    private String _message;
    public ExceptionA(String message)
    {
        _message = message;
    }
    public String getMessage()
    {
        return _message;
    }
}
