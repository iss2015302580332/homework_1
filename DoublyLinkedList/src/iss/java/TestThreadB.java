package iss.java;

/**
 * Created by Weizehao on 16/9/29.
 */
import iss.java.list.MyList;
import iss.java.list.Node;
import iss.java.list.ExceptionA;
import java.util.concurrent.CyclicBarrier;

public class TestThreadB extends Thread
{
    private MyList _list;               // 线程操作的链表
    private Node _node;                 // 被插入/删除位置前方节点
    private CyclicBarrier _barrier;     // 屏障
    private int _choice;                // 选择(插入/删除)
    private int _data;                  // 插入节点的数据
    private String _name;               // 线程名

    /*启动插入线程的构造函数*/
    public TestThreadB(MyList list, Node node, CyclicBarrier barrier, int choice, int data, String name)
    {
        _list = list;
        _node = node;
        _barrier = barrier;
        _choice = choice;
        _data = data;
        _name = name;
    }

    /*启动删除线程的构造函数*/
    public TestThreadB(MyList list, Node node, CyclicBarrier barrier, int choice, String name)
    {
        _list = list;
        _node = node;
        _barrier = barrier;
        _choice = choice;
        _name = name;
    }

    /*
     * 复写run方法
     *
     * 测试说明：
     * 初始节点数有3个
     * 三个线程同时进行插入操作，共插入三百万个节点，插入结束后，总节点数为3000003
     * 三个线程同时进行删除操作，每个线程删除999990个节点，删除结束后，总节点数应为33
     * PS:由于操作的节点数略多，所以测试程序运行稍慢
     */
    public void run()
    {
        //该线程进行插入操作
        if(_choice == 1)
        {
            for(int i = 0; i < 1000000; i++)
            {
                try
                {
                    // 插入元素
                    _list.insert(_node,_data);
                }
                catch (ExceptionA e)
                {
                    System.out.println(e.getMessage());
                }
                finally
                {
                }
            }// end for
            // 线程完成插入工作后，输出当前list元素个数
            System.out.println(_name + " has finished inserting " + _list.getSize());
            try
            {
                // 等待其他线程完成工作
                _barrier.await();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }// end if

        // 该线程进行删除操作
        else if(_choice == 2)
        {
            for(int i = 0; i < 999990; i++)
            {
                try
                {
                    // 此过程涉及读写共享数据的操作，需要保护
                    synchronized ("")
                    {
                        _list.remove(_node.getNext());
                    }
                }
                catch (ExceptionA e)
                {
                    System.out.println(e.getMessage());
                }
                finally
                {

                }
            }// end for
            // 线程完成删除工作后，输出当前list元素个数
            System.out.println(_name + " has finished removing " + _list.getSize());
            try
            {
                // 等待其他线程完成工作
                _barrier.await();
            }
            catch (Exception e)
            {
                e.printStackTrace();;
            }
        }// end else if
    }// end method run
}// end class
