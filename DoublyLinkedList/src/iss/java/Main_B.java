package iss.java;

/**
 * Created by wenke on 2016/9/16.
 * Modified by WeiZehao on 2016/9/29
 */
import iss.java.list.ExceptionA;
import iss.java.list.MyList;
import iss.java.list.Node;
import java.util.concurrent.CyclicBarrier;

public class Main_B {
    public static void main(String[] args)
    {
        MyList list = new MyList();

        // 插入三个初始元素
        try
        {
            list.insert(list.getHead(),0);
            list.insert(list.getHead(),0);
            list.insert(list.getHead(),0);
        }
        catch (ExceptionA e)
        {
            System.out.println(e.getMessage());
        }

        // 声明循环屏障
        CyclicBarrier barrier1 = new CyclicBarrier(4);
        CyclicBarrier barrier2 = new CyclicBarrier(4);
        int count1 = 0;
        int count2 = 0;

        /*声明三个插入线程*/
        TestThreadB thread1 = new TestThreadB(list,list.getHead().getNext(),
                barrier1,1,1,"thread1");
        TestThreadB thread2 = new TestThreadB(list,list.getHead().getNext().getNext(),
                barrier1,1,2,"thread2");
        TestThreadB thread3 = new TestThreadB(list,list.getHead().getNext().getNext().getNext(),
                barrier1,1,3,"thread3");

        /*声明三个删除线程*/
        TestThreadB thread4 = new TestThreadB(list,list.getHead().getNext(),
                barrier2,2,"thread4");
        TestThreadB thread5 = new TestThreadB(list,list.getHead().getNext().getNext(),
                barrier2,2,"thread5");
        TestThreadB thread6 = new TestThreadB(list,list.getHead().getNext().getNext().getNext(),
                barrier2,2,"thread6");

        /*
         * 开始测试，测试的具体方法见TestThreadB.java
         * 开启3个插入线程
         */
        thread1.start();
        thread2.start();
        thread3.start();

        try
        {
            barrier1.await();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        /*开启3个删除线程*/
        thread4.start();
        thread5.start();
        thread6.start();

        try
        {
            barrier2.await();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        // 输出Size值所记录的元素个数
        System.out.println("Size值：" + list.getSize());

        /*正向遍历*/
        for (Node n = list.getHead().getNext(); n!=list.getTail(); n=n.getNext())
        {
            count1++;
            System.out.printf("%d ", n.getData());
        }
        System.out.println();
        // 输出正向遍历后得到的节点数
        System.out.println("正向遍历节点数：" + count1);

        /*逆向遍历*/
        for (Node n = list.getTail().getPrev(); n!=list.getHead(); n=n.getPrev())
        {
            count2++;
            System.out.printf("%d ", n.getData());
        }
        System.out.println();
        // 输出逆向遍历后得到的节点数
        System.out.println("逆向遍历节点数：" + count2);

        /* 以下是对requirementC的测试
        MyList listA = new MyList();
        MyList listB = new MyList();
        try
        {
            //初始化链表内容
            listA.insert(listA.getHead(),0);
            listA.insert(listA.getHead(),1);
            listA.insert(listA.getHead(),2);
            listB.insert(listB.getHead(),0);
            listB.insert(listB.getHead(),1);
            listB.insert(listB.getHead(),2);
            //开始测试
            listA.remove(listB.getHead().getNext());
        }
        catch (ExceptionA e)
        {
            System.out.println(e.getMessage());
        }
        try
        {
            listB.insert(listA.getHead().getNext(),1);
        }
        catch (ExceptionA e)
        {
            System.out.println(e.getMessage());
        }
        */
    }// end main
}// end class
