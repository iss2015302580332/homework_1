package iss.java;

/**
 * Created by wenke on 2016/9/16.
 * modified by WeiZehao on 2016/9/29.
 */
import iss.java.list.Node;

public class Main_A
{
    public static void main(String[] args)
    {
        //生成一个Node对象
        Node node = new Node(null).setData(0);

        /* 创建并启动三个线程
         * 相关测试的方法和说明请查看TestThreadA.java
         */
        TestThreadA Thread1 = new TestThreadA("Thread1",node);
        TestThreadA Thread2 = new TestThreadA("Thread2",node);
        TestThreadA Thread3 = new TestThreadA("Thread3",node);
        Thread1.start();
        Thread2.start();
        Thread3.start();
    }// end main
}// end class Main_A
